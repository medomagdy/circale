package com.circale.android.circale;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements MyRecyclerAdapter.ListItemClickListener {
    private static final String myURL = "https://api.androidhive.info/contacts/";
    private RequestQueue requestQueue;
    private StringRequest request;
    private ArrayList<ContactsClass> contactList;
    private RecyclerView contactsRecyclerView;
    private MyRecyclerAdapter mAdapter;
    private static int NUM_LIST_ITEMS = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        contactList = new ArrayList<ContactsClass>();
        requestQueue = Volley.newRequestQueue(this);
        getContacts();

    }

    private void getContacts() {
        request = new StringRequest(Request.Method.GET, myURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    parseJSonResponse(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, "Error, Check your connection", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<String, String>();
                return hashMap;
            }
        };
        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 5000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 3;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        requestQueue.add(request);
    }

    private void parseJSonResponse(JSONObject response) {
        if (response == null || response.length() == 0) {
            Toast.makeText(MainActivity.this, "Not Found", Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            JSONArray contacts = response.getJSONArray("contacts");
            for (int i = 0; i < contacts.length(); i++) {
                JSONObject c = contacts.getJSONObject(i);
                ContactsClass contact = new ContactsClass();
                contact.setId(c.getString("id"));
                contact.setName(c.getString("name"));
                contact.setAddress(c.getString("address"));
                contact.setEmail(c.getString("email"));
                contact.setGender(c.getString("gender"));
                JSONObject phone = c.getJSONObject("phone");
                contact.setMobile( phone.getString("mobile"));
                contactList.add(contact);
                NUM_LIST_ITEMS ++;
            }

            contactsRecyclerView = (RecyclerView) findViewById(R.id.rv_contacts);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            contactsRecyclerView.setLayoutManager(layoutManager);
            contactsRecyclerView.setHasFixedSize(true);
            mAdapter = new MyRecyclerAdapter(this, contactList);
            contactsRecyclerView.setAdapter(mAdapter);




        } catch (JSONException e) {

            Toast.makeText(MainActivity.this,"Error:" + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onListItemClick(int itemId) {
        Intent intent = new Intent(getBaseContext(), ContactViewActivity.class);
        String c = new Gson().toJson(contactList.get(itemId));
        intent.putExtra("MyClass", c);
        startActivity(intent);
    }
}
