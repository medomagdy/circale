package com.circale.android.circale;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

/**
 * Created by it on 12/11/2017.
 */

public class ContactViewActivity extends AppCompatActivity {

    TextView tv_contact_name;
    TextView tv_contact_email;
    TextView tv_contact_address;
    TextView tv_contact_phone;
    TextView tv_contact_gender;
    Button btnBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_contact);

        Bundle bundle = getIntent().getExtras();
        String c = bundle.getString("MyClass");
        Gson gson = new Gson();
        Type type = new TypeToken<ContactsClass>() {}.getType();
        ContactsClass cont = gson.fromJson(c, type);

        tv_contact_name = (TextView) findViewById(R.id.contact_name);
        tv_contact_email = (TextView) findViewById(R.id.contact_email);
        tv_contact_address = (TextView) findViewById(R.id.contact_address);
        tv_contact_phone = (TextView) findViewById(R.id.contact_mobile);
        tv_contact_gender = (TextView) findViewById(R.id.contact_gender);
        btnBack = (Button) findViewById(R.id.btnBack);


        tv_contact_name.setText(cont.getName().toString());
        tv_contact_email.setText(cont.getEmail().toString());
        tv_contact_address.setText(cont.getAddress().toString());
        tv_contact_phone.setText(cont.getMobile().toString());
        tv_contact_gender.setText(cont.getId().toString());

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
