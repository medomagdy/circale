package com.circale.android.circale;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by it on 12/11/2017.
 */

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.contactViewHolder>  {
    final private ListItemClickListener mOnClickListener;
    private static int viewHolderCount;
    private ArrayList<ContactsClass> contactList;
    private int mNumberItems;

    public MyRecyclerAdapter(ListItemClickListener mOnClickListener, ArrayList<ContactsClass> lst) {
        this.mOnClickListener = mOnClickListener;
        contactList = lst;
        mNumberItems = lst.size();
        viewHolderCount = 0;
    }

    public interface ListItemClickListener{
        public void onListItemClick(int itemId);
    }

    @Override
    public contactViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        int layoutIdForListItem = R.layout.contact_list_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately);
        contactViewHolder viewHolder = new contactViewHolder(view);

        viewHolder.tv_contact_id.setText("ViewHolder index: " + contactList.get(viewHolderCount).getId());

        viewHolderCount++;

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(contactViewHolder holder, int position) {
        holder.bind(contactList.get(position));
    }

    @Override
    public int getItemCount() {
        return mNumberItems;
    }

    class contactViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_contact_id;
        TextView tv_contact_name;
        TextView tv_contact_email;
        TextView tv_contact_address;
        TextView tv_contact_phone;

        public contactViewHolder(View itemView) {
            super(itemView);
            tv_contact_id = (TextView) itemView.findViewById(R.id.tv_contact_id);
            tv_contact_name = (TextView) itemView.findViewById(R.id.tv_contact_name);
            tv_contact_email = (TextView) itemView.findViewById(R.id.tv_contact_email);
            tv_contact_address = (TextView) itemView.findViewById(R.id.tv_contact_address);
            tv_contact_phone = (TextView) itemView.findViewById(R.id.tv_contact_mobile);
            itemView.setOnClickListener(this);
        }
        void bind(ContactsClass cont) {
            tv_contact_id.setText(cont.getId().toString());
            tv_contact_name.setText(cont.getName().toString());
            tv_contact_email.setText(cont.getEmail().toString());
            tv_contact_address.setText(cont.getAddress().toString());
            tv_contact_phone.setText(cont.getMobile().toString());
        }
        @Override
        public void onClick(View view) {
            mOnClickListener.onListItemClick(getAdapterPosition());
        }
    }
}
